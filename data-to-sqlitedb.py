"""
Module that creates a SQLite database from CSV or JSON file.

Author: @1jorge.echeverria
sys.argv[1]: CSV/JSON file path.
sys.argv[2]: SQLite DB path.
"""

from pathlib import Path
import pandas as pd
import sqlite3
import json
import sys


def csv_to_sqlite():
    """
    Creates a SQLite database from CSV file.
    """
    
    df = pd.read_csv(sys.argv[1], delimiter=';')

    connection = sqlite3.connect(sys.argv[2])
    
    df.to_sql('datos', connection, index=False, if_exists='replace')
    
    connection.close()

def json_to_sqlite():
    """
    Creates a SQLite database from JSON file.
    """

    with open(sys.argv[1], 'r', encoding='utf-8') as file:
        data = json.load(file)

    df = pd.DataFrame(data)

    with sqlite3.connect(sys.argv[2], detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as connection:
        df.to_sql('datos', connection, if_exists='replace', index=False)

if __name__ == "__main__":
    extension = Path(sys.argv[1]).suffix
    
    if extension == '.csv':
        csv_to_sqlite()
    elif extension == '.json':
        json_to_sqlite()
