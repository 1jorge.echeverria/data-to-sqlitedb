# data-to-sqlitedb

## Description

Module that creates a SQLite database from CSV or JSON file.

## Pip Packages

- pandas
- sqlite3

## Terminal Syntax

```
Example with CSV file: python data-to-sqlitedb.py <CSV file> <DB file>
Example with JSON file: python data-to-sqlitedb.py <JSON file> <DB file>
```
